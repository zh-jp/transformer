# 目录释义
- `read_world_example.py`: 使用 Transformer 模型在“德语 -> 英语的翻译”任务上训练。
- `training.py`: Transformer 训练策略，包括：Loss 函数，正则化方法，Decode策略，训练函数等。
- `training.py`: Transformer 模型架构。
- `backend.py`: real_world_example.py 训练后产出 checkpoint， 该文件测试单个德语句子输入后的翻译结果。
- `test_model.ipynb`: 同 backend.py。
- `test_model.ipynb`: 对Transformer 部分设置的可视化，如掩码设置，标签平滑等。

# 参考文献
1. [annotated-transformer](https://github.com/harvardnlp/annotated-transformer)
